package com.ezcart.authentication.config;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;



import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import reactor.core.publisher.Mono;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ezcart.authentication.helper.AuthenticationHelper;
import com.ezcart.authentication.model.User;
import com.ezcart.authentication.repository.UserRepository;


@Component
public class JWTUtil {
	
	@Value("${springbootjjwt.jjwt.secret}")
	private String secret;
	
	@Value("${springbootjjwt.jjwt.expiration}")
	private String expirationTime;

	private Key key;
	
	@Autowired
	AuthenticationHelper authHelper;
	
	@Autowired
	UserRepository userRepository;

	@PostConstruct
	public void init(){
		this.key = Keys.hmacShaKeyFor(secret.getBytes());
	}

	public Claims getAllClaimsFromToken(String token) {
		return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
	}
	
	public String getUsernameFromToken(String token) {
		return getAllClaimsFromToken(token).getSubject();
	}
	
	public Date getExpirationDateFromToken(String token) {
		return getAllClaimsFromToken(token).getExpiration();
	}
	
	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}
	
	public Mono<Map<String,String>> generateToken(String encodeUsernamePassword) {
		User user = authHelper.getUser(encodeUsernamePassword);
		Map<String,String> userDetails = new HashMap<>();
		if(!StringUtils.isEmpty(user)) {
		Map<String, Object> claims = new HashMap<>();
		claims.put("role", user.getRole());
		String token = doGenerateToken(claims, user.getUsername());
		userDetails.put("loggedName", user.getUsername());
		userDetails.put("token", token);
		return Mono.just(userDetails);
		}
		else {
		return null;
		}
	}

	private String doGenerateToken(Map<String, Object> claims, String username) {
		Long expirationTimeLong = Long.parseLong(expirationTime); //in second
		
		final Date createdDate = new Date();
		final Date expirationDate = new Date(createdDate.getTime() + expirationTimeLong * 1000);

		return Jwts.builder()
				.setClaims(claims)
				.setSubject(username)
				.setIssuedAt(createdDate)
				.setExpiration(expirationDate)
				.signWith(key)
				.compact();
	}
	
	public Boolean validateToken(String token) {
		return !isTokenExpired(token);
	}

}
