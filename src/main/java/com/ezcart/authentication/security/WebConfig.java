package com.ezcart.authentication.security;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.reactive.config.EnableWebFlux;

import com.ezcart.authentication.handler.ForbiddenHandler;
import com.ezcart.authentication.handler.UnauthorizedHandler;


@Configuration
@EnableWebFluxSecurity
public class WebConfig extends WebSecurityConfigurerAdapter {

	@Bean
    public UnauthorizedHandler unauthorizedHandler() throws Exception {
        return new UnauthorizedHandler();
	}
	
	@Bean
    public ForbiddenHandler forbiddenHandler() throws Exception {
        return new ForbiddenHandler();
    }
	
	@Bean
    public SecurityConfig authenticationFilterBean() throws Exception {
        return new SecurityConfig();
    }
	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
			.csrf().disable()
			
			.exceptionHandling().authenticationEntryPoint(unauthorizedHandler()).and()
			.exceptionHandling().accessDeniedHandler(forbiddenHandler()).and()
			
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			
			.authorizeRequests()
			
			.antMatchers("/auth/login").permitAll()
			
			.antMatchers("/signup/user").permitAll()
			
			.antMatchers("/signup/dealer").permitAll()
			
			.antMatchers("/auth/loginByOTP").permitAll()
			
			.anyRequest().authenticated();
		
		// custom JWT based security filter
		httpSecurity.addFilterBefore(authenticationFilterBean(), UsernamePasswordAuthenticationFilter.class);

		httpSecurity.headers().cacheControl();
	}
}
