package com.ezcart.authentication.repository;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.ezcart.authentication.model.User;

import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveCrudRepository<User ,Integer>{

	@Query(value = "select * from user where email = ?emailID and password =?password")
	Mono<User> findByEmailidAndPassword(String emailID,String password);
	
	@Query(value = "select * from user where contactNumber = ?contactNumber and password =?password")	
	Mono<User> findByContactNumberAndPassword(String contactNumber,String password);
	
	@Query("select * from user where email = ?emailID")
	Mono<User>  findByEmailId(String emailId);

	@Query("select * from user where contactNumber = ?contactNumber")
	Mono<User>  findByContactNumber(String contactNumber);

}
