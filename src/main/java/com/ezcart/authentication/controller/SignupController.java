package com.ezcart.authentication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ezcart.authentication.helper.AuthenticationHelper;
import com.ezcart.authentication.model.User;

import reactor.core.publisher.Mono;

@CrossOrigin
@RestController
@RequestMapping("/signup")
public class SignupController {

	@Autowired
	AuthenticationHelper authHelper;
	
	@PostMapping("/user")
	public ResponseEntity<Mono<User>> signUpDetails(@RequestBody User user ){
		return authHelper.signUp(user);
	}

	@PostMapping("/dealer")
	public ResponseEntity<Mono<User>> signUpDealer(@RequestBody User user ){
		return authHelper.signUpDealer(user);
	}
}
