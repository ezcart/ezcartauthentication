package com.ezcart.authentication.controller;

import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ezcart.authentication.helper.AuthenticationHelper;
import com.ezcart.authentication.model.User;

import reactor.core.publisher.Mono;

@CrossOrigin
@RestController
@RequestMapping("/auth")
public class AuthenticationController {
	
	@Autowired
	com.ezcart.authentication.config.JWTUtil jwtUtil;
	
	@Autowired
	AuthenticationHelper authHelper;

	@RequestMapping("/login")
	public ResponseEntity<Mono<Map<String, String>>> loginValidation(@RequestHeader("Authorization") String authHeader){
		if(!StringUtils.isEmpty(jwtUtil.generateToken(authHeader))) {
			return ResponseEntity.ok(jwtUtil.generateToken(authHeader));
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@RequestMapping("/loginByOTP")
	public Mono<String> loginOTPGenerate(){
		return authHelper.loginOTPGeneration();
	}

}
