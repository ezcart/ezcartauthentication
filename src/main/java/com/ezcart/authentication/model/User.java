package com.ezcart.authentication.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table("user")
public class User implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private String username;

	@Id
	private Integer uno;
	
	private String password;

	@Getter
	@Setter
	private String role;
	
	@Getter
	@Setter
	private String email;
	
	@Getter
	@Setter
	private String addressLine;
	
	@Getter
	@Setter
	private String city;
	
	@Getter
	@Setter
	private String state;
	
	@Getter
	@Setter
	private String country;
	
	@Getter
	@Setter
	@Column(value = "contactNumber")
	private String contactNumber;

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(role.toString()));
		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

}
