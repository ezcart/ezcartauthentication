package com.ezcart.authentication.helper;

import java.util.Base64;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.ezcart.authentication.model.User;
import com.ezcart.authentication.repository.UserRepository;

import reactor.core.publisher.Mono;


@Service
public class AuthenticationHelper {

	@Autowired
	UserRepository userRepository;
	

	public User getUser(String encodeUsernamePassword) {
		String usernameAndPassword[] = getUsername(encodeUsernamePassword).split(":");
		User user = new User();
		if(usernameAndPassword[0].contains("@"))
			user = userRepository.findByEmailidAndPassword(usernameAndPassword[0],usernameAndPassword[1]).block();
		else
			user = userRepository.findByContactNumberAndPassword(usernameAndPassword[0],usernameAndPassword[1]).block();
		if(!StringUtils.isEmpty(user))
			return user;
		else
			return null;
	}
	
	private String getUsername(String authHeader) {
		 String encoded = authHeader.substring(6).toString();
		 Base64.Decoder decoder = Base64.getMimeDecoder();  
		 String decoded = new String(decoder.decode(encoded)); 
		 return decoded;
	 }

	public ResponseEntity<Mono<User>> signUp(User user) {
		// TODO Auto-generated method stub
		user.setRole("user");
		if(userRepository.findByEmailId(user.getEmail()).block()!=null)
			return ResponseEntity.status(HttpStatus.valueOf("Email Already Exist")).build();
		else if(userRepository.findByContactNumber(user.getContactNumber()).block()!=null)
			return ResponseEntity.status(HttpStatus.valueOf("Contact Number Already Exist")).build();
		else
			return ResponseEntity.ok(userRepository.save(user));
	}

	public ResponseEntity<Mono<User>> signUpDealer(User user) {
		// TODO Auto-generated method stub
		user.setRole("dealer");
		if(userRepository.findByEmailId(user.getEmail()).block()!=null)
			return ResponseEntity.status(HttpStatus.valueOf("Email Already Exist")).build();
		else if(userRepository.findByContactNumber(user.getContactNumber()).block()!=null)
			return ResponseEntity.status(HttpStatus.valueOf("Contact Number Already Exist")).build();
		else
			return ResponseEntity.ok(userRepository.save(user));

	}

	public Mono<String> loginOTPGeneration() {
		// TODO Auto-generated method stub
		Random random = new Random();
		String otp = "";
		for(int irr = 0 ; irr < 5 ;irr++)
			otp+=String.valueOf(random.nextInt(10));
		System.err.println("\n\n Generated OTP is : "+otp+"\n\n");
		return Mono.just(otp);
	}
}
