package com.ezcart.authentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(scanBasePackages = {"com.ezcart.authentication.config","com.ezcart.authentication.controller","com.ezcart.authentication.handler","com.ezcart.authentication.helper","com.ezcart.authentication.model","com.ezcart.authentication.repository"},
exclude = { SecurityAutoConfiguration.class})
public class EzCartAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(EzCartAuthenticationApplication.class, args);
	}

}
